package game;

import audio.Muzyka;
import entity.mob.Player;
import graphics.Screen;
import input.Keyboard;
import input.Mouse;
import level.Level;
import level.LevelChange;
import level.Poziomy.Stage1;
import level.Poziomy.Stage2;
import level.SpawnLevel;
import level.TileCoordinate;
import level.tile.Tile;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;


/**
 * Represents the Game.
 *
 * @author Artur Kisicki
 * @version 1.1
 * @since 1.0
 */
public final class Game extends Canvas implements Runnable {


    //do krecenai sie
    public static int cameraShiftX = 0, cameraShiftY = 0;

    private static int width = 400;   //400
    private static int height = width * 9 / 16;
    private static int scale = 4; //4

    private Thread thread;
    private JFrame frame;
    //do krecenai sie
    double alfa = 0;
    private Muzyka music;
    public static Level level;
    public Stage1 stage1;
    private Stage2 stage2;
    public static Player player;
    private LevelChange levelChange;
    public static boolean running = false;
    private long gameTime;
    private Keyboard key;
    private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

    private Screen screen;

    private static String tilte = "Gra";

    private static final Game INSTANCE = new Game();
    private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

    /**
     * Returns instance of game
     *
     * @return instance of the game
     */
    public static Game getInstance() {
        return INSTANCE;
    }

    /**
     * Get width of actually used window.
     *
     * @return width of window
     */
    public static int getWindowWidth() {
        return width * scale;
    }

    /**
     * Get height of actually used window.
     *
     * @return height of window
     */
    public static int getWindowHeight() {
        return height * scale;
    }

    private Game() {

        Dimension size = new Dimension(width * scale, height * scale);
        setPreferredSize(size);
        Tile.setupMap();
        screen = new Screen(width, height);
        frame = new JFrame();
        //level = new SpawnLevel("res/textures/randomLevel.png", "res/textures/randomLevelObj.png");

        level = new SpawnLevel("res/textures/stage1.png", "res/textures/stage1Objects.png");
        TileCoordinate playerSpawn = new TileCoordinate(28, 38); //28 36   // 13 13

        key = new Keyboard();
        addKeyListener(key);
        Mouse mouse = new Mouse();
        addMouseListener(mouse);
        addMouseMotionListener(mouse);

        player = new Player(playerSpawn.x(), playerSpawn.y(), key);
        player.init(level);

        stage1 = new Stage1();
        //stage2 = new Stage2();
        Muzyka.play(2);
        levelChange = new LevelChange();

    }

    /**
     * Forces game to stop.
     */
    public synchronized void stop() {
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calls all elements of the game like keys, player or mobs to update. It's for checking position, attack, movement and other stuff.
     */
    private void update() {
        key.update();
        player.update();
        level.update();
        levelChange.update();


    }

    /**
     * Game loop
     */
    public void run() {
        gameTime = System.currentTimeMillis();

        long lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        final double ns = 1000000000.0 / 60.0;
        double delta = 0;
        int frames = 0;
        int updates = 0;

        //requestFocus();
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while (delta >= 1) {
                update();
                updates++;

                delta--;
            }
            render();
            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                frame.setTitle(tilte + " | " + updates + " ups, " + frames + "  fps   x: " + player.x / 16 + " y: " + player.y / 16);
                updates = 0;
                frames = 0;

            }
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        frame.dispose();
        //new GameOverFrame(System.currentTimeMillis() - gameTime);
    }

    /**
     * Starts game as a thread
     */
    public synchronized void start() {
        running = true;
        thread = new Thread(this, "Display");
        thread.start();
    }

    /**
     * Prepare all elements of game to be rendered and showed to gamer.
     */
    public void render() {

        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }
        screen.clear();
        int xScroll = player.x - (screen.width / 2);
        int yScroll = player.y - (screen.height / 2);

        level.render(xScroll + cameraShiftX, yScroll + cameraShiftY, screen);
        player.render(screen);
        level.renderLight(screen);
        for (int i = 0; i < pixels.length; i++)
            pixels[i] = screen.pixels[i];

        Graphics g = bs.getDrawGraphics();                                        //Hmmmmmmmm
        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        g.dispose();
        bs.show();
    }

    /**
     * Set game's frame to given.
     *
     * @param frame given frame
     */
    public void setFrame(JFrame frame) {
        this.frame = frame;
        frame.add(this);
        adjustFrame();
    }

    /**
     * Adjust actual game's frame.
     */
    public void adjustFrame() {
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
}
