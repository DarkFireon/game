package game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;

public class GameOverFrame extends JFrame {

    public static boolean wygrana;
    private JButton nextButton = new JButton("Next");
    private JButton exitButton = new JButton("EXIT");
    private ArrayList<String> napisyKoncowe = new ArrayList<String>();
    private String text;
    private int index = 0;
    private String pathToGame;
    private String pathToImage = "\\res\\images\\";
    private JLabel label = new JLabel("Witaj w naszej grze !");
    private long czas;

    /**
     * creating Game over Menu
     *
     * @param czas variable thats tell how much time we spend in game
     */
    public GameOverFrame(long czas) {

        try {
            pathToGame = new java.io.File(".").getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.czas = czas;
        //this.wygrana=wygrana;

        setTitle("Dzieki za gre");
        setSize(800, 800);
        setLocation(new Point(600, 100));
        setLayout(null);
        setResizable(false);

        initComponent();
        initEvent();
        setVisible(true);

        napisyKoncowe.add("<HTML>");
        napisyKoncowe.add("<h1 align='center'>Głowny programista : </h1>");
        napisyKoncowe.add("<h1 align='center'>Sponsor : </h1>");
        napisyKoncowe.add("<h1 align='center'>Głowny grafik gry : </h1>");
        napisyKoncowe.add("<h1 align='center'>Fabuła : </h1>");
        napisyKoncowe.add("<h1 align='center'>Studenci wykorzystani przy produkcji :</h1>");
        napisyKoncowe.add("<h1 align='center'>aktor głosowy :  </h1>");
        napisyKoncowe.add("<h1 align='center'>Powod depresji : </h1>");
        napisyKoncowe.add("<h1 align='center'>95% napisanego kodu : </h1>");
        napisyKoncowe.add("<h1 align='center'>szczegolne podziekowania dla : </h1>");
        napisyKoncowe.add("<h1 align='center'>koniec : </h1>");
    }


    private void initComponent() {

        label.setBounds(150, 20, 600, 600);

        String text;
        if (wygrana)
            text = "<HTML><h1 align='center'>Udało ci sie wygrac w czasie :";
        else
            text = "<HTML><h1 align='center'>Przegrales w : ";
        text += czas / 1000;
        text += " sekund<br/> </h1>";
        //text += "<img src=\"file:\\D:\\pulpit\\mem.png\" height=\"400\" width=\"500\" align='center' >";
        text += "<img src=\"file:\\" + pathToGame + pathToImage + "victory.jpg\"  height=\"400\" width=\"500\" align='center'>";


        label.setText(text);
        label.setFont((new Font("Courier New", Font.BOLD, 16)));


        nextButton.setBounds(150, 600, 200, 100);
        nextButton.setBackground(Color.getHSBColor(124, 321, 153));
        nextButton.setFont((new Font("Courier New", Font.BOLD, 16)));

        exitButton.setBounds(450, 600, 200, 100);
        exitButton.setBackground(Color.getHSBColor(124, 151, 35));
        exitButton.setFont((new Font("Courier New", Font.BOLD, 16)));


        add(nextButton);
        add(exitButton);
        add(label);
    }


    private void initEvent() {

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });

        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exit(e);
            }
        });

        nextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                next(e);
            }
        });

    }

    private void exit(ActionEvent evt) {

        System.exit(0);
    }

    private void next(ActionEvent evt) {
        if (index + 1 < napisyKoncowe.size())
            index++;

        text = napisyKoncowe.get(0) + napisyKoncowe.get(index);
        text += "<img src=\"file:\\" + pathToGame + pathToImage + "zdjecie" + (index) + ".jpg\"  height=\"500\" width=\"500\" align='center'>";

        label.setForeground(Color.red);
        label.setText(text);
    }


}
