package game;

import javax.swing.*;
import java.awt.*;

public class StartGame extends Canvas {

    /**
     * Creates JFrame to run a game inside.
     *
     * @param args outside args to that program
     */
    public static void main(String[] args) {

        // new StartFrame();


        JFrame frame = new JFrame();
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        Game.getInstance().setFrame(frame);
        Game.getInstance().start();


    }
}
