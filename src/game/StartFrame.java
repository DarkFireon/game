package game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class StartFrame extends JFrame {

    private JButton startButton = new JButton("START GAME");


    private JLabel label = new JLabel("Witaj w naszej grze !");

    /**
     * creating Start Menu
     */
    public StartFrame() {
        setTitle("Nasza Gra");
        setSize(600, 600);
        setLocation(new Point(600, 200));
        setLayout(null);
        setResizable(false);

        initComponent();
        initEvent();
        setVisible(true);
    }

    private void initComponent() {


        String text;

        text = "<HTML><h1 align='center'>Najleszpa gra ever</h1></br>";
        text += "Witaj w naszej grze !<br/<br/><br/>";
        text += "Aby wygrac gre musisz zabic wszystkie potwory na zewnatrz oraz w piwnicy<br/><br/>";


        label.setText(text);
        label.setOpaque(false);
        label.setFont((new Font("Courier New", Font.BOLD, 16)));
        label.setBounds(60, 100, 480, 180);

        startButton.setBounds(100, 400, 400, 100);
        startButton.setBackground(Color.getHSBColor(124, 151, 35));
        startButton.setFont((new Font("Courier New", Font.BOLD, 16)));


        add(startButton);
        add(label);
    }

    private void initEvent() {

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });

        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startButtonClick(e);
            }
        });

    }

    private void startButtonClick(ActionEvent evt) {
        JFrame frame = new JFrame();
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        Game.getInstance().setFrame(frame);
        Game.getInstance().start();

        dispose();

    }


}
