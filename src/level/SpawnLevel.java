package level;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SpawnLevel extends Level {


    public SpawnLevel(String path, String pathObjects) {
        super(path, pathObjects);

    }

    /**
     * loads the level from two pictures
     *
     * @param path        map of the level
     * @param pathObjects map with objects
     */
    protected void loadLevel(String path, String pathObjects) {

        try {
            BufferedImage image = ImageIO.read(new File(path));
            int w = width = image.getWidth();
            int h = height = image.getHeight();
            tiles = new int[w * h];
            image.getRGB(0, 0, w, h, tiles, 0, w);

        } catch (IOException e) {
            System.out.println("Could not load LEVEL file");
            e.printStackTrace();

        }

        try {
            BufferedImage image = ImageIO.read(new File(pathObjects));
            int w = width = image.getWidth();
            int h = height = image.getHeight();
            tiles1 = new int[w * h];
            image.getRGB(0, 0, w, h, tiles1, 0, w);

        } catch (IOException e) {
            System.out.println("Could not load LEVEL entity file");
            e.printStackTrace();

        }
    }

    protected void generateLevel() {
    }


}
