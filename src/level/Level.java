package level;

import entity.mob.Mob;
import entity.projectile.Projectile;
import game.Game;
import game.GameOverFrame;
import graphics.Screen;
import graphics.Sprite;
import level.tile.Tile;
import utilities.Vector2i;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static level.LevelChange.stageTwo;

public class Level {

    private final int tilesAround = 9;

    protected int width, height;

    protected int[] tiles;                 //przechowuje szblony np tiles[1]=trawa [2]=kamien itp
    public static List<Mob> entities = new ArrayList<Mob>();

    private int time = 0;
    public List<Mob> presentEntities = new ArrayList<Mob>();
    protected int[] tiles1;
    private static List<Projectile> projectiles = new ArrayList<Projectile>();

    /**
     * Creates game level with the given width and heigt of map
     *
     * @param width
     * @param height
     */
    public Level(int width, int height) {
        this.width = width;
        this.height = height;
        tiles = new int[width * height];
        tiles1 = new int[width * height];
        generateLevel();
    }

    /**
     * Creates game level with the given width and heigt of map
     *
     * @param path
     * @param path1
     */
    public Level(String path, String path1) {
        loadLevel(path, path1);
        generateLevel();

    }

    /**
     * removing a mob from level
     *
     * @param i index of the mob
     */
    public static void removeMob(int i) {
        entities.get(i).death();
        entities.remove(i);
    }

    protected void loadLevel(String path, String path1) {
    }

    /**
     * adding a mob to the level
     *
     * @param mob
     */
    public static void add(Mob mob) {
        entities.add(mob);
    }

    /**
     * @return return projectiles
     */
    public List<Projectile> getProjectiles() {
        return projectiles;
    }

    /**
     * Check if there is collision with a tile
     *
     * @param x    beginning position
     * @param y    beginning position
     * @param xa   shift
     * @param ya   shift
     * @param size size of the tile
     * @return true if there is collision
     */
    public boolean tileCollision(double x, double y, double xa, double ya, int size) {
        boolean solid = false;

        for (int corner = 0; corner < 4; corner++) {
            double xt = (((int) x + (int) xa) + ((corner % 2) * size - 3)) / 16;
            double yt = (((int) y + (int) ya) + (corner / 2) * size + 3) / 16;
            if (getTile((int) xt, (int) yt).solid()) solid = true;
            if (getTileObject((int) xt, (int) yt).solid()) solid = true;
        }
        return solid;
    }

    protected void generateLevel() {
    }

    /**
     * Compare two nodes by their fCost
     */
    private Comparator<Node> nodeSorter = new Comparator<Node>() {
        public int compare(Node n0, Node n1) {
            if (n1.fCost < n0.fCost) return 1;
            if (n1.fCost > n0.fCost) return -1;
            return 0;
        }
    };

    /**
     * update entities and projectiles and tests if how many enemies is still alive
     */
    public void update() {

        for (int i = 0; i < entities.size(); i++)
            if (entities.get(i).getStageNumber() == Game.player.getStageNumber())
                entities.get(i).update();

        for (int i = 0; i < projectiles.size(); i++) {
            projectiles.get(i).update();
        }

        System.out.println(entities.size());
        if (entities.size() == 0 && stageTwo) {                                //HMmmmmmmmmmmmm
            GameOverFrame.wygrana = true;
            Game.running = false;
        }

    }

    /**
     * render level
     *
     * @param xScroll position of the player
     * @param yScroll position of the player
     * @param screen
     */
    public void render(int xScroll, int yScroll, Screen screen) {
        screen.setOffset(xScroll, yScroll);
        int x0 = xScroll >> 4;                                 //lewa strona ekranu|dzielimy przez 16 chcemy zajmowac sie tailami
        int x1 = (xScroll + screen.width + 16) >> 4;            //prawa strona ekranu
        int y0 = yScroll >> 4;
        int y1 = (yScroll + screen.width) >> 4;

        for (int y = y0; y < y1; y++) {                         //lecimy od lewej gory do prawego dolu ekranu i uzupelnia wszystko
            for (int x = x0; x < x1; x++) {
                getTile(x, y).render(x, y, screen);
                getTileObject(x, y).render(x, y, screen);
            }
        }

        for (int i = 0; i < entities.size(); i++)
            if (entities.get(i).getStageNumber() == Game.player.getStageNumber())
                entities.get(i).render(screen);


        for (int i = 0; i < projectiles.size(); i++) {
            projectiles.get(i).render(screen);
        }
    }

    /**
     * render light around you and dim light elsewhere
     *
     * @param screen
     */
    public void renderLight(Screen screen) {
        screen.renderLight();
    }

    /*
    void setNewPresentList(int previousStage, int nextStage) {
        for (int i = 0; i < entities.size(); i++)
            if (entities.get(i).getStageNumber() == previousStage)
                entities.remove(i);

        for (int i = 0; i < presentEntities.size(); i++)
            entities.add(presentEntities.get(i));

        presentEntities.clear();

        for (int i = 0; i < entities.size(); i++)
            if (entities.get(i).getStageNumber() == nextStage)
                presentEntities.add(entities.get(i));
    }*/

    /**
     * Add projectile to the level
     *
     * @param p procjetile
     */
    public void addProjectile(Projectile p) {
        p.init(this);
        projectiles.add(p);
    }

    /**
     * zwraca cala droge powstala na wskutek funkcji findPath
     *
     * @param openList   czyscimy openListe
     * @param closedList czyscimy closedListe
     * @param current    za pomoca tego punktu rekonstrujemy droge
     */
    private List<Node> reconstructRoad(List<Node> openList, List<Node> closedList, Node current) {
        List<Node> path = new ArrayList<Node>();
        while (current.parent != null) {
            path.add(current);
            current = current.parent;
        }
        openList.clear();
        closedList.clear();
        return path;
    }

    /**
     * find a path beetween two points
     *
     * @param start first point
     * @param goal  second point
     */

    public List<Node> findPath(Vector2i start, Vector2i goal) {
        List<Node> openList = new ArrayList<Node>();
        List<Node> closedList = new ArrayList<Node>();
        Node current = new Node(start, null, 0, getDistance(start, goal));
        openList.add(current);

        while (openList.size() > 0) {

            Collections.sort(openList, nodeSorter);
            current = openList.get(0);
            if (current.tile.equals(goal)) {
                return reconstructRoad(openList, closedList, current);
            }
            openList.remove(current);
            closedList.add(current);
            for (int i = 0; i < tilesAround; i++) {
                if (i == 4) continue;
                int x = current.tile.getX();
                int y = current.tile.getY();
                int xi = (i % 3) - 1;
                int yi = (i / 3) - 1;
                Tile tile = getTile(x + xi, y + yi);
                Tile tileObject = getTileObject(x + xi, y + yi);
                if (tile == null) continue;
                if (tile.solid()) continue;
                if (tileObject.solid()) continue;
                Vector2i vector = new Vector2i(x + xi, y + yi);
                double gCost = current.gCost + getDistance(current.tile, vector);
                double hCost = getDistance(vector, goal);
                Node node = new Node(vector, current, gCost, hCost);
                if (vecInList(closedList, vector))
                    continue;
                if (!vecInList(openList, vector))
                    openList.add(node);
            }

        }
        closedList.clear();
        return null;
    }

    /**
     * Measure actual distance between vectors
     *
     * @param tile wektor poczatkowy
     * @param goal wektor koncowy
     * @return distance
     */
    private double getDistance(Vector2i tile, Vector2i goal) {
        double dx = tile.getX() - goal.getX();
        double dy = tile.getY() - goal.getY();
        return Math.sqrt(dx * dx + dy * dy);

    }

    /**
     * Find out of there is a vector in a list
     *
     * @param list   list vectors
     * @param vector vector to test
     * @return true if there is vector like this in list
     */
    private boolean vecInList(List<Node> list, Vector2i vector) {
        for (Node n : list) {
            if (n.tile.equals(vector)) return true;
        }
        return false;
    }

    /**
     * Find out of there is a vector in a list
     *
     * @param x position
     * @param y position
     * @return tileObject from position
     */
    public Tile getTileObject(int x, int y) {
        int wspolrzedne = x + y * width;

        if (x < 0 || y < 0 || x >= width || y >= height) return Tile.getTile(-1);  //voidTile !!
        return Tile.getTile(tiles1[wspolrzedne]);
    }

    /**
     * Find out of there is a vector in a list
     *
     * @param x position
     * @param y position
     * @return tile from position
     */
    public Tile getTile(int x, int y) {
        time++;
        if (time % 4220 == 0) {
            Sprite.water.update();
            Tile.getWaterTile().updateTile(Sprite.water.getSprite());
        }

        if (x < 0 || y < 0 || x >= width || y >= height)
            return Tile.getBlackTile();

        int wspolrzedne = x + y * width;
        return Tile.getTile(tiles[wspolrzedne]);

    }
    /**
     * clear projectiles on the map
     */
    void clearProjectiles() {
        projectiles.clear();
    }


}
