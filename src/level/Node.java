package level;

import utilities.Vector2i;

public class Node {
    public Vector2i tile;
    public Node parent;
    public double fCost, gCost, hCost;

    /**
     * Creates a Node
     *
     * @param tile   lokalizacja wezla
     * @param parent
     * @param gCost  koszty przejsca od poprzedniego wezla
     * @param hCost  koszty przejscia do celu
     */
    public Node(Vector2i tile, Node parent, double gCost, double hCost) {
        this.tile = tile;
        this.parent = parent;
        this.hCost = hCost;
        this.gCost = gCost;
        this.fCost = this.gCost + this.hCost;
    }
}
