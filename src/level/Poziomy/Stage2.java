package level.Poziomy;

import entity.mob.enemy.Boss;
import entity.mob.enemy.NightKing;
import entity.mob.enemy.RedSkeleton;
import entity.mob.enemy.Skeleton;
import level.Level;

public class Stage2 {

    /**
     * Load content of 2nd stage.
     */
    public Stage2() {


        Level.add(new Skeleton(51, 50, 2));
        Level.add(new Skeleton(51, 51, 2));


        //1 sala
        Level.add(new RedSkeleton(78, 33, 2));
        Level.add(new RedSkeleton(80, 33, 2));
        Level.add(new RedSkeleton(88, 33, 2));
        Level.add(new RedSkeleton(81, 22, 2));
        Level.add(new RedSkeleton(88, 23, 2));
        Level.add(new RedSkeleton(88, 23, 2));


        //2 sala
        Level.add(new NightKing(83, 69, 2));
        Level.add(new NightKing(82, 69, 2));
        Level.add(new Skeleton(85, 71, 2));
        Level.add(new Skeleton(78, 71, 2));


        //3 sala
        Level.add(new Boss(38, 89, 2));
        Level.add(new RedSkeleton(41, 92, 2));
        Level.add(new RedSkeleton(42, 92, 2));


        //4 sala
        Level.add(new Boss(10, 11, 2));
        Level.add(new Boss(11, 11, 2));
        Level.add(new NightKing(12, 11, 2));
        for (int i = 0; i < 10; i++)
            Level.add(new Skeleton(9, 10 + i, 2));


    }
}
