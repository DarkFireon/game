package level.Poziomy;

import entity.mob.enemy.NightKing;
import entity.mob.enemy.Skeleton;
import level.Level;

public class Stage1 {


    /**
     * Load content of 1st stage.
     */
    public Stage1() {
        // Level.add(new RedSkeleton(27, 28), 1);

        // Level.add(new RedSkeleton(29, 28), 1);

        Level.add(new Skeleton(29, 28, 1));
        Level.add(new NightKing(28, 28, 1));
    }
}
