package level;

import game.Game;
import level.Poziomy.Stage2;

public class LevelChange {

    private int x;
    private int y;

    private final TileCoordinate LEVEL_1 = new TileCoordinate(33, 24);
    private final TileCoordinate LEVEL_2 = new TileCoordinate(32, 20);

    public static boolean stageTwo = false;


    public LevelChange() {

    }


    /**
     * checks if level should be changed
     */
    public void update() {
        x = Game.player.x;
        y = Game.player.y;


        if (x >= LEVEL_1.x() - 3 && x <= LEVEL_1.x() + 1 && y >= LEVEL_1.y() && y <= LEVEL_1.y() + 16 && Game.player.getStageNumber() == 1)                                  // Z pierwsego na drugi
        {
            Game.player.setStageNumber(2);
            Level level = Game.level = new SpawnLevel("res/textures/stage2.png", "res/textures/stage2Objects.png");
            Game.player.teleport(LEVEL_2.x() - 16, LEVEL_2.y());
            Game.player.init(level);
            level.clearProjectiles();
            initStageTwo();
            Game.player.clothsChange();
        }

        if (x >= LEVEL_2.x() - 3 && x <= LEVEL_2.x() + 3 && y >= LEVEL_2.y() - 3 && y <= LEVEL_2.y() + 2 && Game.player.getStageNumber() == 2)                                  // Z pierwsego na drugi
        {
            Game.player.setStageNumber(1);
            Level level = Game.level = new SpawnLevel("res/textures/stage1.png", "res/textures/stage1Objects.png");
            Game.player.teleport(LEVEL_1.x() + 16, LEVEL_1.y());
            Game.player.init(level);
            level.clearProjectiles();

        }

    }


    /**
     * initiates and adds a enemies to the level
     */
    void initStageTwo() {
        if (!stageTwo) {
            Stage2 stage2 = new Stage2();
            stageTwo = true;
        }
    }


}
