package level.tile;

import graphics.Screen;
import graphics.Sprite;

import java.util.Map;
import java.util.TreeMap;

//zaladowanie tila
public class Tile {

    public int x, y;
    public Sprite sprite;


    private static Map<Integer, Tile> tilesMap = new TreeMap<Integer, Tile>() {
    };
    private static Tile waterTileAnim = new WallTile(Sprite.water.getSprite());

    /**
     * Create tile from sprite
     */
    public Tile(Sprite sprite) {
        this.sprite = sprite;
    }

    public static Tile getTile(int color) {

        if (tilesMap.containsKey(color))
            return tilesMap.get(color);

        return tilesMap.get(-1);
    }

    public static Tile getWaterTile() {
        return waterTileAnim;
    }

    public static Tile getBlackTile() {
        return Tile.getTile(0xff000000);
    }

    /**
     * Setting up a Map of the all Tiles
     */
    public static void setupMap() {
        tilesMap.put(0xff000000, new WallTile(Sprite.black));
        tilesMap.put(0xff00ff00, new TloTile(Sprite.grass));
        tilesMap.put(0xffffd800, new TloTile(Sprite.road));
        tilesMap.put(0xff0000ff, new WallTile(Sprite.water.getSprite()));
        tilesMap.put(0xffc43d19, new WallTile(Sprite.lava));
        tilesMap.put(0xff5c5854, new TloTile(Sprite.floor1));
        tilesMap.put(0xff7b4300, new WallTile(Sprite.wall));
        tilesMap.put(0xff6b3a00, new WallTile(Sprite.wallFront));
        tilesMap.put(0xff542e00, new WallTile(Sprite.wallCorner));
        tilesMap.put(0xff964c95, new WallTile(Sprite.rift1));

        tilesMap.put(0xffa49543, new TloTile(Sprite.floor));
        tilesMap.put(0xffff8a00, new WallTile(Sprite.bridgePion));
        tilesMap.put(0xffff7300, new TloTile(Sprite.bridge1Poziom));
        tilesMap.put(0xffff0000, new TloTile(Sprite.stairsDown));
        tilesMap.put(0xffff6464, new TloTile(Sprite.stairsUp));
        tilesMap.put(0xffc4a600, new WallTile(Sprite.chest));
        tilesMap.put(0xff00747b, new TloTile(Sprite.door));
        tilesMap.put(0xff156300, new ObjectTile(Sprite.tree));

        tilesMap.put(-1, new TloTile(Sprite.void1Sprite));


    }

    /**
     * Changing tile
     *
     * @param newSprite new sprite
     */
    public void updateTile(Sprite newSprite) {
        sprite = newSprite;
    }

    /**
     * function which render a tile
     */
    public void render(int x, int y, Screen screen) {
        screen.renderTile(x << 4, y << 4, this);
    }


    /**
     * returns if Tile if solid
     */
    public boolean solid() {
        return false;
    }
}
