package level.tile;

import graphics.Sprite;

public class VoidTile extends Tile {
    public VoidTile(Sprite sprite) {
        super(sprite);
    }


    public boolean solid(){
        return true;
    }
}
