package level.tile;

import graphics.Sprite;

public class WallTile extends Tile {
    public WallTile(Sprite sprite) {           //pozwala extends Tile bo nie ma default contructora
        super(sprite);
    }


    public boolean solid(){
        return true;
    }
}
