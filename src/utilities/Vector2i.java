package utilities;

public class Vector2i {

    private int x, y;

    public Vector2i(int x, int y) {
        set(x, y);
    }

    public Vector2i() {
        set(0, 0);
    }

    public Vector2i(Vector2i vector2i) {
        this.x = vector2i.x;
        this.y = vector2i.y;
    }


    public void set(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void add(Vector2i vector2i) {
        this.x += vector2i.x;
        this.y += vector2i.y;
    }

    public boolean equals(Object object) {
        if (!(object instanceof Vector2i)) return false;
        Vector2i vec = (Vector2i) object;
        return vec.getX() == this.getX() && vec.getY() == this.getY();
    }

}
