package input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Mouse implements MouseListener, MouseMotionListener {

    private static int mouseX = -1;
    private static int mouseY = -1;
    private static int mouseB = -1;

    /**
     * Returns the position's x coordinate of mouse click on the screen.
     */
    public static int getX() {
        return mouseX;
    }

    /**
     * Returns the position's y coordinate of mouse click on the screen.
     */
    public static int getY() {
        return mouseY;
    }

    /**
     * Returns the number of clicked mouse button (left | right).
     */
    public static int getButton() {
        return mouseB;
    }

    /**
     * This method handle mouse clicked event and free mouseB.
     */
    public void mouseClicked(MouseEvent e) {
        mouseB = -1;
    }

    /**
     * This method handle mouse pressed event and set mouseB.
     */
    public void mousePressed(MouseEvent e) {
        mouseB = e.getButton();
    }

    /**
     * This method handle mouse released event and set mouseB to null.
     */
    public void mouseReleased(MouseEvent e) {
        mouseB = -1;
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }


    /**
     * This method handle mouse dragged event and set mouse pointer coordinates.
     */
    public void mouseDragged(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
    }

    /**
     * This method handle mouse moved event and set mouse pointer coordinates.
     */
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
    }
}
