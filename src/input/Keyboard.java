package input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    public boolean up, down, left, right, shift, space;
    private boolean[] keys = new boolean[120];

    public Keyboard() {
    }

    /**
     * This method check if the arrows or shift keys were stroked.
     */
    public void update() {
        up = keys[KeyEvent.VK_UP] || keys[KeyEvent.VK_W];
        down = keys[KeyEvent.VK_DOWN] || keys[KeyEvent.VK_S];
        left = keys[KeyEvent.VK_LEFT] || keys[KeyEvent.VK_A];
        right = keys[KeyEvent.VK_RIGHT] || keys[KeyEvent.VK_D];
        shift = keys[KeyEvent.VK_SHIFT];
        space = keys[KeyEvent.VK_SPACE];
    }

    public void keyTyped(KeyEvent e) {

    }

    /**
     * This method handle key pressed event.
     */
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
    }

    /**
     * This method handle key released event.
     */
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }
}
