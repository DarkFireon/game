package graphics;

import entity.mob.Mob;
import entity.mob.enemy.Boss;
import entity.mob.enemy.NightKing;
import entity.mob.enemy.RedSkeleton;
import entity.projectile.Projectile;
import game.Game;
import level.tile.Tile;

import java.util.Random;


public class Screen {
    public final int MAP_SIZE = 64;
    public int[] pixels;
    public final int MAP_SIZE_MASK = MAP_SIZE - 1;
    public int width, height;
    public int xOffset, yOffset;

    public Random random = new Random();
    public int[] tiles = new int[64 * 64];

    /**
     * Create Screen for game usage.
     *
     * @param width  screen width
     * @param height screen height
     */
    public Screen(int width, int height) {
        this.width = width;
        this.height = height;
        pixels = new int[width * height];

        for (int i = 0; i < 64 * 64; i++)
            tiles[i] = random.nextInt(0xFFFFFF);
        tiles[0] = 0;
    }

    /**
     * Clear all pixels located on the screen
     */
    public void clear() {

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++)
                pixels[x + y * width] = 0;

        }
    }

    /**
     * Renders tile at given position
     *
     * @param xp   position's coordinate
     * @param yp   position's coordinate
     * @param tile tile to render
     */
    public void renderTile(int xp, int yp, Tile tile) {
        //xa=x absoulte    //x,y piksel   yp=pozycja tila

        xp -= xOffset;
        yp -= yOffset;
        for (int y = 0; y < tile.sprite.SIZE; y++) {
            int ya = y + yp;
            for (int x = 0; x < tile.sprite.SIZE; x++) {
                int xa = x + xp;
                if (xa < -tile.sprite.SIZE || xa >= width || ya < 0 || ya >= height)
                    break;     // bez -tile widzimy renderowanie na ekranie
                if (xa < 0) xa = 0;
                int col = tile.sprite.pixels[x + y * tile.sprite.SIZE];
                if (col != 0xffff00ff)
                    pixels[xa + ya * width] = tile.sprite.pixels[x + y * tile.sprite.SIZE];


            }
        }
    }

    /**
     * Renders projectile at given position
     *
     * @param xp position's coordinate
     * @param yp position's coordinate
     * @param p  projectile to render
     */
    public void renderProjectile(int xp, int yp, Projectile p) {
        //xa=x absoulte    //x,y piksel   yp=pozycja tila
        xp -= xOffset;
        yp -= yOffset;
        for (int y = 0; y < p.getSpriteSize(); y++) {
            int ya = y + yp;
            for (int x = 0; x < p.getSpriteSize(); x++) {
                int xa = x + xp;
                if (xa < -p.getSpriteSize() || xa >= width || ya < 0 || ya >= height)
                    break;     // bez -tile widzimy renderowanie na ekranie
                if (xa < 0) xa = 0;
                int col = p.getSprite().pixels[x + y * p.getSpriteSize()];
                if (col != 0xffff00ff) pixels[xa + ya * width] = p.getSprite().pixels[x + y * p.getSprite().SIZE];
                //lokacja w grze (offset!)      lokacja piksel z tila bez offseta
            }
        }
    }


    /**
     * Renders mob at given position
     *
     * @param xp  position's coordinate
     * @param yp  position's coordinate
     * @param mob mob to render
     */
    public void renderMob(int xp, int yp, Mob mob) {
        //xa=x absoulte    //x,y piksel   yp=pozycja tila
        xp -= xOffset;
        yp -= yOffset;
        for (int y = 0; y < 16; y++) {
            int ya = y + yp;
            for (int x = 0; x < 16; x++) {
                int xa = x + xp;
                if (xa < -16 || xa >= width || ya < 0 || ya >= height)
                    break;     // bez -tile widzimy renderowanie na ekranie
                if (xa < 0) xa = 0;
                if (y == 0) {
                    for (int i = 15; i > 0; i--) {
                        xa = i + xp;
                        if (xa < 0) xa = 0;
                        if (xa < -16 || xa >= width || ya < 0 || ya >= height)
                            break;     // bez -tile widzimy renderowanie na ekranie
                        if (mob.getHP() < ((i + 1) * (mob.getMAX_HP() / 16)))
                            pixels[xa + ya * width] = 0xffff0000;
                        else {
                            pixels[xa + ya * width] = 0xff00ff00;
                        }
                    }
                } else {
                    //lokacja w grze (offset!)      lokacja piksel z tila bez offseta
                    int col = mob.getSprite().pixels[x + y * 16];
                    if (mob instanceof RedSkeleton && col == 0xff8595a1) col = 0xffb5220e;
                    if (mob instanceof Boss && col == 0xff8595a1) col = 0xff00ff00;
                    if (mob instanceof NightKing && col == 0xff8595a1) col = 0xff0000ff;
                    if (col != 0xffff00ff) pixels[xa + ya * width] = col;
                }
            }
        }
    }


    /**
     * Renders player at given position
     *
     * @param xp     position's coordinate
     * @param yp     position's coordinate
     * @param sprite concret sprite to use
     * @param mob    player to render
     */
    public void renderMob(int xp, int yp, Sprite sprite, Mob mob) {
        //xa=x absoulte    //x,y piksel   yp=pozycja tila
        xp -= xOffset;
        yp -= yOffset;
        for (int y = 0; y < sprite.SIZE; y++) {
            int ya = y + yp;
            for (int x = 0; x < sprite.SIZE; x++) {
                int xa = x + xp;
                if (xa < -sprite.SIZE || xa >= width || ya < 0 || ya >= height)
                    break;
                if (xa < 0) xa = 0;
                if (x == 0) {
                    if (mob.getHP() < ((y + 1) * (mob.getMAX_HP() / 16)))
                        pixels[xa + ya * width] = 0xffff0000;
                    else {
                        pixels[xa + ya * width] = 0xff00ff00;
                    }
                } else {
                    ya = y + yp;
                    int col = sprite.pixels[x + y * sprite.SIZE];
                    if (col != 0xffff00ff) pixels[xa + ya * width] = col;
                }
            }
        }
    }


    public void renderLight() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                double distance = Math.sqrt(Math.pow(x - (width / 2 + 8), 2) + Math.pow(y - (height / 2 + 8), 2));

                if (distance > 0) {
                    Color color = new Color(pixels[x + y * width]);
                    if (Game.player.getStageNumber() == 1)
                        color.changeForDarker((int) (distance * 0.9));
                    if (Game.player.getStageNumber() == 2)
                        color.changeForDarker((int) (distance * 1.65));
                    pixels[x + y * width] = color.getInt();
                }

            }
        }
    }


    /**
     * Renders sprite at given position
     *
     * @param xp     position's coordinate
     * @param yp     position's coordinate
     * @param sprite sprite to render
     * @param fixed  checks if sprite is going to be fixed on the screen
     */
    public void renderSprite(int xp, int yp, Sprite sprite, boolean fixed) {
        if (fixed) {
            xp -= xOffset;
            yp -= yOffset;
        }
        for (int y = 0; y < sprite.getHeight(); y++) {
            int ya = y + yp;
            for (int x = 0; x < sprite.getWidth(); x++) {
                int xa = x + xp;
                if (xa < 0 || xa >= width || ya < 0 || ya >= height) continue;
                pixels[xa + ya * width] = sprite.pixels[x + y * sprite.getWidth()];
            }
        }
    }

    /**
     * Set offset of game screen
     *
     * @param xOffset x offset value
     * @param yOffset y offset value
     */
    public void setOffset(int xOffset, int yOffset) {
        this.xOffset = xOffset;
        this.yOffset = yOffset;

    }


}
