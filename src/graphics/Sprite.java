package graphics;

public class Sprite {

    public final int SIZE;
    private int x, y;
    public static Sprite void1Sprite = new Sprite(16, 0xffff00ff);
    public int[] pixels;
    protected SpriteSheet sheet;
    //  TLA
    public static Sprite grass = new Sprite(16, 2, 10, SpriteSheet.basicTiles);
    public static Sprite road = new Sprite(16, 2, 1, SpriteSheet.basicTiles);
    public static Sprite floor = new Sprite(16, 0, 2, SpriteSheet.basicTiles);
    public static Sprite floor1 = new Sprite(16, 6, 1, SpriteSheet.basicTiles);
    public static Sprite black = new Sprite(16, 6, 2, SpriteSheet.basicTiles);
    // INNE
    public static Sprite bridgePion = new Sprite(16, 0, 11, SpriteSheet.basicTiles);
    public static Sprite bridge1Poziom = new Sprite(16, 1, 10, SpriteSheet.basicTiles);
    public static Sprite chest = new Sprite(16, 4, 4, SpriteSheet.basicTiles);
    public static Sprite door = new Sprite(16, 0, 6, SpriteSheet.basicTiles);
    public static Sprite stairsDown = new Sprite(16, 1, 7, SpriteSheet.basicTiles);
    public static Sprite stairsUp = new Sprite(16, 0, 7, SpriteSheet.basicTiles);
    public static Sprite tree = new Sprite(16, 4, 9, SpriteSheet.basicTiles);
    // SCIANY
    public static Sprite wall = new Sprite(16, 1, 0, SpriteSheet.basicTiles);
    public static Sprite wallFront = new Sprite(16, 0, 0, SpriteSheet.basicTiles);
    public static Sprite wallCorner = new Sprite(16, 3, 0, SpriteSheet.basicTiles);
    public static Sprite rift1 = new Sprite(16, 0, 13, SpriteSheet.basicTiles);
    public static Sprite rift2 = new Sprite(16, 1, 13, SpriteSheet.basicTiles);
    // PLYNY
    public static AnimatedSprite water = new AnimatedSprite(SpriteSheet.water, 16, 16, 5);
    public static Sprite lava = new Sprite(16, 2, 9, SpriteSheet.basicTiles);
    //postacie
    public static Sprite skeleton = new Sprite(16, 10, 0, SpriteSheet.charactersTiles);
    public static Sprite fireball = new Sprite(6, 0, 0, SpriteSheet.projcetiles);


    // Projectiles
    public static Sprite evilFireball = new Sprite(6, 1, 0, SpriteSheet.projcetiles);
    private final int width, height;


    protected Sprite(SpriteSheet sheet, int width, int height) {
        SIZE = (width == height) ? width : -1;
        this.width = width;
        this.height = height;
        this.sheet = sheet;

    }

    /**
     * Creates a Sprite instance on the given sheet with the given size and at the given position.
     *
     * @param sheet sheet of sprite
     * @param x     position coordinate
     * @param y     position coordinate
     * @param sheet given sheet
     */
    public Sprite(int size, int x, int y, SpriteSheet sheet) {
        SIZE = size;
        width = size;
        height = size;
        pixels = new int[SIZE * SIZE];
        this.x = x * SIZE;
        this.y = y * SIZE;
        this.sheet = sheet;
        load();
    }

    /**
     * Creates a single color Sprite instance.
     *
     * @param size   size of sprite
     * @param coulor value of colour to set
     */
    public Sprite(int size, int coulor) {
        SIZE = size;
        this.width = size;
        this.height = size;
        pixels = new int[SIZE * SIZE];
        setColour(coulor);
    }

    /**
     * Creates a single color Sprite instance.
     *
     * @param width  width of sprite
     * @param height height of sprite
     * @param coulor value of colour to set
     */
    public Sprite(int width, int height, int coulor) {
        SIZE = -1;
        this.width = width;
        this.height = height;
        pixels = new int[height * width];
        setColour(coulor);
    }

    /**
     * Creates a single color Sprite instance.
     *
     * @param pixels exact array of pixels which craetes sprite
     * @param width  width of sprite
     * @param height height of sprite
     */
    public Sprite(int[] pixels, int width, int height) {
        SIZE = (width == height) ? width : -1;
        this.width = width;
        this.height = height;
        this.pixels = pixels;

    }


    private void setColour(int coulor) {
        for (int i = 0; i < width * height; i++)
            pixels[i] = coulor;
    }

    //KONIEC JEDNO KOLOROWY SPRITE

    private void load() {

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixels[x + y * width] = sheet.pixels[(x + this.x) + (y + this.y) * sheet.SIZE_X];
            }
        }
    }

    /**
     * Returns height of Sprite instance.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns width of Sprite instance.
     */
    public int getWidth() {
        return width;
    }
}


