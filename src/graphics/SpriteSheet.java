package graphics;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SpriteSheet {
    private String path;
    private int SIZE;
    public int[] pixels;
    public static SpriteSheet charactersTiles = new SpriteSheet("res/textures/characters.png", 192, 128);
    public static SpriteSheet basicTiles = new SpriteSheet("res/textures/basictiles.png", 128, 240);
    protected SpriteSheet sheet;
    public static SpriteSheet projcetiles = new SpriteSheet("res/textures/fireball.png", 96, 96);
    public static SpriteSheet waterSpriteSheet = new SpriteSheet("res/textures/water.png", 480, 48);
    public static SpriteSheet water = new SpriteSheet(waterSpriteSheet, 0, 0, 16, 1, 16);
    public static SpriteSheet player_up = new SpriteSheet(charactersTiles, 0, 3, 3, 1, 16);
    public static SpriteSheet player_down = new SpriteSheet(charactersTiles, 0, 0, 3, 1, 16);
    public static SpriteSheet player_left = new SpriteSheet(charactersTiles, 0, 1, 3, 1, 16);
    public static SpriteSheet player_right = new SpriteSheet(charactersTiles, 0, 2, 3, 1, 16);
    public static SpriteSheet dummy_up = new SpriteSheet(charactersTiles, 9, 3, 3, 1, 16);
    public static SpriteSheet dummy_down = new SpriteSheet(charactersTiles, 9, 0, 3, 1, 16);

    public static SpriteSheet player2_up = new SpriteSheet(charactersTiles, 3, 3, 3, 1, 16);
    public static SpriteSheet player2_down = new SpriteSheet(charactersTiles, 3, 0, 3, 1, 16);
    public static SpriteSheet player2_left = new SpriteSheet(charactersTiles, 3, 1, 3, 1, 16);
    public static SpriteSheet player2_right = new SpriteSheet(charactersTiles, 3, 2, 3, 1, 16);
    public static SpriteSheet dummy_left = new SpriteSheet(charactersTiles, 9, 1, 3, 1, 16);
    public static SpriteSheet dummy_right = new SpriteSheet(charactersTiles, 9, 2, 3, 1, 16);
    public int SIZE_X;
    public int SIZE_Y;


    private Sprite[] sprites;

    /**
     * Creates a SpriteSheet instance on the given sheet, with the given width, height, size and on the given position
     *
     * @param sheet      sheet of sprite
     * @param x          position's coordinate
     * @param y          position's coordinate
     * @param width      width of sheet
     * @param height     height of sheet
     * @param spriteSize game sprite size (NxN)
     */
    public SpriteSheet(SpriteSheet sheet, int x, int y, int width, int height, int spriteSize) {
        int xx = x * spriteSize;
        int yy = y * spriteSize;
        int w = width * spriteSize;
        int h = height * spriteSize;
        SIZE_X = w;
        SIZE_Y = h;
        if (width == height) SIZE = SIZE_X;
        else SIZE = -1;
        pixels = new int[w * h];
        for (int y0 = 0; y0 < h; y0++) {
            int yp = yy + y0;
            for (int x0 = 0; x0 < w; x0++) {
                int xp = xx + x0;
                pixels[x0 + y0 * w] = sheet.pixels[xp + yp * sheet.SIZE_X];
            }
        }
        int frame = 0;
        sprites = new Sprite[width * height];
        for (int xa = 0; xa < width; xa++) {
            for (int ya = 0; ya < height; ya++) {
                int[] spritePixels = new int[spriteSize * spriteSize];
                for (int y0 = 0; y0 < spriteSize; y0++) {
                    for (int x0 = 0; x0 < spriteSize; x0++) {
                        spritePixels[x0 + y0 * spriteSize] = pixels[(x0 + xa * spriteSize) + (y0 + ya * spriteSize) * SIZE_X];
                    }
                }
                Sprite sprite = new Sprite(spritePixels, spriteSize, spriteSize);
                sprites[frame++] = sprite;
            }
        }
    }


    /**
     * Creates a SpriteSheet instance on the given sheet, with the given width, height, size and on the given position
     *
     * @param sheet      sheet of sprite
     * @param x          position's coordinate
     * @param y          position's coordinate
     * @param width      width of sheet
     * @param height     height of sheet
     * @param spriteSize game sprite size (N)
     * @param spriteSize game sprite size (M)
     */
    public SpriteSheet(SpriteSheet sheet, int x, int y, int width, int height, int spriteSize, int spriteSize2) {
        int xx = x * spriteSize;
        int yy = y * spriteSize2;
        int w = width * spriteSize;
        int h = height * spriteSize2;
        SIZE_X = w;
        SIZE_Y = h;
        if (width == height) SIZE = SIZE_X;
        else SIZE = -1;
        pixels = new int[w * h];
        for (int y0 = 0; y0 < h; y0++) {
            int yp = yy + y0;
            for (int x0 = 0; x0 < w; x0++) {
                int xp = xx + x0;
                pixels[x0 + y0 * w] = sheet.pixels[xp + yp * sheet.SIZE_X];
            }
        }
        int frame = 0;
        sprites = new Sprite[width * height];
        for (int xa = 0; xa < width; xa++) {
            for (int ya = 0; ya < height; ya++) {
                int[] spritePixels = new int[spriteSize * spriteSize];
                for (int y0 = 0; y0 < spriteSize; y0++) {
                    for (int x0 = 0; x0 < spriteSize; x0++) {
                        spritePixels[x0 + y0 * spriteSize] = pixels[(x0 + xa * spriteSize) + (y0 + ya * spriteSize) * SIZE_X];
                    }
                }
                Sprite sprite = new Sprite(spritePixels, spriteSize, spriteSize);
                sprites[frame++] = sprite;
            }
        }
    }


    /**
     * Creates a SpriteSheet instance from the fiven path which reffers to local map file. It also set size.
     *
     * @param path   path to local map file
     * @param SIZE_X game sprite size (N)
     * @param Size_Y game sprite size (M)
     */
    public SpriteSheet(String path, int SIZE_X, int Size_Y) {
        this.path = path;
        this.SIZE_X = SIZE_X;
        this.SIZE_Y = Size_Y;
        pixels = new int[SIZE_X * SIZE_Y];
        load(path);
    }


    /**
     * Returns list of sprites located on the sheet
     */
    public Sprite[] getSprite() {
        return sprites;
    }

    private void load(String path) {
        try {
            BufferedImage image = ImageIO.read(new File(path));
            int w = image.getWidth();
            int h = image.getHeight();
            image.getRGB(0, 0, w, h, pixels, 0, w);

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

}
