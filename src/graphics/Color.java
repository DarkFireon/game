package graphics;

public class Color {
    int alpha, red, green, blue;


    public Color(int color) {
        blue = (color) & 0xFF;
        green = (color >> 8) & 0xFF;
        red = (color >> 16) & 0xFF;
        alpha = (color >> 24) & 0xFF;
    }

    void changeColor(int color) {
        blue = (color) & 0xFF;
        green = (color >> 8) & 0xFF;
        red = (color >> 16) & 0xFF;
        alpha = (color >> 24) & 0xFF;
    }


    void changeForDarker(int amount) {

        if (red - amount < 0) {
            red = 0;
        } else {
            red -= amount;
        }
        if (green - amount < 0) {
            green = 0;
        } else {
            green -= amount;
        }
        if (blue - amount < 0) {
            blue = 0;
        } else {
            blue -= amount;
        }
    }

    public int getInt() {
        int argb = (alpha << 24) | (red << 16) | (green << 8) | blue;
        return argb;
    }

}
