package graphics;

public class AnimatedSprite extends Sprite {


    private int frame = 0;
    private Sprite sprite;
    private int animationSize = 0;
    private int rate = 10;
    private int time = 0;
    private int lenght = -1;

    /**
     * Creates a AnimatedSprite instance on the given sheet, with the given width, height and length
     *
     * @param sheet  sheet of sprite
     * @param width  width of sprite
     * @param height height of sprite
     * @param lenght length of sprite
     */
    public AnimatedSprite(SpriteSheet sheet, int width, int height, int lenght) {
        super(sheet, width, height);
        this.lenght = lenght;
        sprite = sheet.getSprite()[0];
        if (lenght > sheet.getSprite().length) System.out.println("error, lenght of animation is too long");
    }

    /**
     * Perform animation for sprite.
     */
    public void update() {
        time++;
        if (time % rate == 0) {
            if (frame >= lenght - 1) frame = 0;
            else frame++;
            sprite = sheet.getSprite()[frame];
        }
    }

    /**
     * Returns sprite isntnace
     */
    public Sprite getSprite() {
        return sprite;
    }

    /**
     * Set frame rate for sprite's animation
     */
    public void setFrameRate(int frames) {
        rate = frames;
    }

    /**
     * Set frame for sprite's animation
     */
    public void setFrame(int index) {
        if (index > sheet.getSprite().length - 1) {
            System.err.println("index out of bounds in: " + this);
            return;
        }
        sprite = sheet.getSprite()[index];
    }
}


