package exceptions;

public class DoubleInstaceException extends Exception {
    /**
     * Creates instance of DoubleInstanceException
     */
    public DoubleInstaceException() {
        super("Game must follow Singleton pattern.");
    }
}
