package entity.projectile;

import entity.Entity;
import graphics.Screen;
import graphics.Sprite;

public class Projectile extends Entity {

    protected final int xOrgin,yOrgin;
    protected double angle;
    protected Sprite sprite;
    protected double nx,ny;
    protected double x,y;
    protected double speed, range;
    protected int damage;

    /**
     * Creates a Projectile instance at given position and with the given angle which points to direction.
     *
     * @param x position
     * @param y position
     * @param dir angle
     */
    public Projectile(int x,int y,double dir){
        xOrgin=x;
        yOrgin=y;
        angle=dir;
        this.x=x;
        this.y=y;
    }

    /**
     * Get actual sprite of projectile.
     */
    public Sprite getSprite(){
        return sprite;
    }
    /**
     * Get size of actual sprite.
     */
    public int getSpriteSize(){
        return sprite.SIZE;
    }

    protected double distance() {
        double distance=Math.sqrt(((xOrgin-x)*(xOrgin-x))+((yOrgin-y)*(yOrgin-y)));
        return distance;
    }
    /**
     * Renders projectile on screen.
     */
    public void render(Screen screen){
        screen.renderProjectile((int)x,(int)y,this);
    }

    /**
     * Updates projectile's position on screen.
     */
    public void update() {
        if(level.tileCollision(x,y,nx,ny,6))remove();
        move();
    }

    protected void move() {
        /*for(int i=0;i<Math.abs(xa);i++)           cos takiego tylko do pociskow
            if (!collisionPlayer(abs(xa),ya))
                x+= abs(xa);
        for(int i=0;i<Math.abs(ya);i++)
            if(!collisionPlayer(xa,abs(ya)))
                y+=abs(ya);*/

        if(!level.tileCollision(x,y,nx,ny,6)) {
            x += nx;
            y += ny;

            if (distance() > range) {
                remove();
            }
            hit();
        }
    }

    protected void hit() {
    }
}
