package entity.projectile;

import game.Game;
import graphics.Sprite;

public class EnemyProjectile extends Projectile {


    public static final int FIRE_RATE = 12;

    /**
     * Creates a EnemyProjectile instance at given position and with the given angle which points to direction.
     *
     * @param x position
     * @param y position
     * @param dir angle
     */
    public EnemyProjectile(int x, int y, double dir) {
        super(x, y, dir);
        range=180;
        damage = 15;
        speed=1.5;
        sprite= Sprite.evilFireball;

        nx=speed*Math.cos(angle);
        ny=speed*Math.sin(angle);
    }

    protected void hit() {
                if (((nx + x) > Game.player.getX()) && ((nx + x) <= Game.player.getX() + 16 && ((ny + y)>=(Game.player.getY())) && ((ny + y)<= (Game.player.getY()+16)))) {
                    Game.player.loseHP(damage);
                    remove();
                }
    }

}

