package entity.projectile;

import game.Game;
import graphics.Sprite;
import level.Level;

public class WizardProjectile extends Projectile {


    //public static final int FIRE_RATE = 15;
    public static final int FIRE_RATE = 6;

    /**
     * Creates a WizardProjectile instance at given position and with the given angle which points to direction.
     *
     * @param x position
     * @param y position
     * @param dir angle
     */
    public WizardProjectile(int x, int y, double dir) {
        super(x, y, dir);
        range=180;
        damage = 2000;
        speed=5;
        sprite= Sprite.fireball;
        //=Sprite.door;

        nx=speed*Math.cos(angle);
        ny=speed*Math.sin(angle);
    }


    protected void hit() {

        for (int i = 0; i < Level.entities.size(); i++) {
            if (Level.entities.get(i).getStageNumber() == Game.player.getStageNumber()) {
                if (((nx + x) > Level.entities.get(i).getX()) && ((nx + x) <= Level.entities.get(i).getX() + 16 && ((ny + y) >= (Level.entities.get(i).getY())) && ((ny + y) <= (Level.entities.get(i).getY() + 16)))) {
                    Level.entities.get(i).loseHP(damage);
                    remove();

                    if (Level.entities.get(i).getHP() <= 0) {
                        Level.removeMob(i);
                    }
                }
            }
        }

    }
}

