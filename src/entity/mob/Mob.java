package entity.mob;

import entity.Entity;
import entity.projectile.EnemyProjectile;
import entity.projectile.Projectile;
import entity.projectile.WizardProjectile;
import game.Game;
import level.Level;


public abstract class Mob extends Entity {
    protected boolean walking = false;
    protected Direction dir;
    protected int HP;
    protected int MAX_HP;

    protected void enemyShoot(int x, int y, double dirProjectile) {

        Projectile p = new EnemyProjectile(x, y, dirProjectile);
        level.addProjectile(p);
    }

    protected void shoot(int x, int y, double dirProjectile) {
        Projectile p = new WizardProjectile(x, y, dirProjectile);
        level.addProjectile(p);
    }

    private int abs(int value) {
        if (value < 0) return -1;
        return 1;
    }

    /**
     * Returns actual amount of health points
     */
    public int getHP() {
        return HP;
    }

    /**
     * Returns maximum amount of health points.
     */
    public int getMAX_HP() {
        return MAX_HP;
    }

    /**
     * Reduces actual health points of Mob instance
     */
    public void loseHP(int damage) {
        HP -= damage;
    }

    /**
     * Heals Mob instance with given amount of health points.
     *
     * @param  rewardHP amount of health points
     */
    public void gainHP(int rewardHP) {
        if (HP + rewardHP <= MAX_HP)
            HP += rewardHP;
        else
            HP = MAX_HP;
    }

    /**
     * Performs death event of this Mob instance.
     */
    public void death() {

    }

    /**
     * Move Mob position on game board
     *
     * @param  xa move vector's x position
     * @param  ya move vector's y position
     */
    public void move(int xa, int ya) {   //xa,ya zmiana delta x
        if (xa != 0 && ya != 0) {
            move(xa, 0);
            move(0, ya);
            return;
        }
        if (xa > 0) dir = Direction.RIGHT;
        if (xa < 0) dir = Direction.LEFT;
        if (ya > 0) dir = Direction.DOWN;
        if (ya < 0) dir = Direction.UP;
        for (int i = 0; i < Math.abs(xa); i++)
            if (!collision(abs(xa), ya))
                x += abs(xa);
        for (int i = 0; i < Math.abs(ya); i++)
            if (!collision(xa, abs(ya)))
                y += abs(ya);
    }

    protected void movePlayer(int xa, int ya) {   //xa,ya zmiana delta x
        if (xa != 0 && ya != 0) {
            movePlayer(xa, 0);
            movePlayer(0, ya);
            return;
        }
        if (xa > 0) dir = Direction.RIGHT;
        if (xa < 0) dir = Direction.LEFT;
        if (ya > 0) dir = Direction.DOWN;
        if (ya < 0) dir = Direction.UP;
        for (int i = 0; i < Math.abs(xa); i++)
            if (!collisionPlayer(abs(xa), ya))
                x += abs(xa);
        for (int i = 0; i < Math.abs(ya); i++)
            if (!collisionPlayer(xa, abs(ya)))
                y += abs(ya);
    }

    protected boolean collision(int xa, int ya) {
        Level level = Game.level;
        Player player = Game.player;
        for (int corner = 0; corner < 4; corner++) {
            int xt = ((x + xa) + ((corner % 2) * 15)) / 16;
            int yt = ((y + ya) + (corner / 2) * 15) / 16;
            //condition with wall condition
            if (level.getTile(xt, yt).solid()) return true;
            //collision with object condition
            if (level.getTileObject(xt, yt).solid()) return true;
            //collision with player condition
            if ((xa + x >= player.getX() - 3 && xa + x <= player.getX() + 3) && (ya + y >= player.getY() - 3 && ya + y <= player.getY() + 3))
                return true;
        }
        return false;
    }

    protected boolean collisionPlayer(int xa, int ya) {
        Level level = Game.level;
        for (int corner = 0; corner < 4; corner++) {
            int xt = 0;
            if (corner == 0 || corner == 2)
                xt = ((x + xa + 3) + ((corner % 2) * 15)) / 16;

            if (corner == 1 || corner == 3)
                xt = ((x + xa - 3) + ((corner % 2) * 15)) / 16;

            int yt = ((y + ya) + (corner / 2) * 15) / 16;
            //condition with wall condition
            if (level.getTile(xt, yt).solid()) return true;
            //collision with player condition
            if (level.getTileObject(xt, yt).solid()) return true;
        }
        return false;
    }

    /**
     * Set new position to the Mob instance
     *
     * @param  x new position's x coordinate
     * @param  y new position's y coordinate
     */
    public void teleport(int x, int y) {
        this.x = x;
        this.y = y;
    }

    protected boolean collisionResp(int x, int y) {
        boolean solid = false;
        Level level = Game.level;
        if (level.getTile(x / 16, y / 16).solid()) solid = true;
        if (level.getTileObject(x / 16, y / 16).solid()) solid = true;
        return solid;
    }

    protected enum Direction {
        UP, DOWN, LEFT, RIGHT
    }
}













