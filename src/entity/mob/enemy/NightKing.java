package entity.mob.enemy;

import graphics.Sprite;
import level.Level;

public class NightKing extends Enemy {

    private int xa = 0, ya = 0;
    private int maxCreation = 55;
    private int time = 60, time1 = 100;

    /**
     * Creates a NightKing instance at given position and at given stage.
     *
     * @param x position
     * @param y position
     * @param stageNumber number of game stage (e.g. dangeons)
     */
    public NightKing(int x, int y, int stageNumber) {
        //init(Game.level);
        this.x = x << 4;
        this.y = y << 4;
        sprite = Sprite.skeleton;
        HP = 100;
        MAX_HP = 100;
        this.stageNumber = stageNumber;
    }

    /**
     * Change king's behaviur on game board. It means it's position and it performs attack on game hero. Method also spawns new skeletons.
     */
    public void update() {
        time--;

        if (walking) animSprite.update();
        else animSprite.setFrame(1);

        if (distanceToPlayer() < 200) {
            time1--;
            if (time1 == 0 && maxCreation > 1) {
                int xt = this.x + (generator.nextInt(3) - 1) * 16;
                int yt = this.y + (generator.nextInt(3) - 1) * 16;
                if (!collisionResp(xt, yt)) {
                    Skeleton skeleton = new Skeleton(xt >> 4, yt >> 4, stageNumber);
                    skeleton.setOwner(this);
                    Level.add(skeleton);
                    maxCreation--;

                }
                time1 = 30;
            }
        }
        if (time == 0) {
            xa = generator.nextInt(3) - 1;
            ya = generator.nextInt(3) - 1;
            time = 60;
            healing();
        }
        walk(xa, ya);
    }

    /**
     * Method to heal NightKing
     */
    public void healing() {
        if (HP + 5 <= MAX_HP)
            HP += 5;
    }

    /**
     * Method to kill NightKing and perform other actions related to hid death. Like death of his servants.
     */
    public void death() {
        for (int i = Level.entities.size() - 1; i > 0; i--) {
            if (Level.entities.get(i) instanceof Skeleton) {
                if (((((Skeleton) Level.entities.get(i))).getOwner()).equals(this)) {
                    Level.removeMob(i);
                }
            }
        }
    }
}

