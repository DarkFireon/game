package entity.mob.enemy;

import entity.projectile.EnemyProjectile;
import game.Game;
import graphics.Sprite;

public class Boss extends Enemy {

    private int xa = 0, ya = 0;
    private int fireRate;
    private int newX;
    private int newY;
    private boolean healingAbility = true;
    private int time = 60, time1 = 0;

    /**
     * Creates a boss instance at given position and at given stage.
     *
     * @param x position
     * @param y position
     * @param stageNumber number of game stage (e.g. dangeons)
     */
    public Boss(int x, int y, int stageNumber) {
        init(Game.level);
        this.x = x << 4;
        this.y = y << 4;
        sprite = Sprite.skeleton;
        HP = 420;
        fireRate = EnemyProjectile.FIRE_RATE;
        MAX_HP = 420;
        this.stageNumber = stageNumber;
    }

    /**
     * Change boss's behaviur on game board. It means it's position and it performs attack on game hero.
     */
    public void update() {
        if (fireRate > 0) fireRate--;
        time--;
        time1++;

        if (walking) animSprite.update();
        else animSprite.setFrame(1);

        lastChance();
        if (distanceToPlayer() < 200) {
            atack();
        }

        if (time == 0) {
            xa = generator.nextInt(3) - 1;
            ya = generator.nextInt(3) - 1;
            time = 30;
        }

        walk(xa, ya);
    }

    private void atack() {
        if (fireRate <= 0) {
            atackPlayer();
            fireRate = EnemyProjectile.FIRE_RATE;
        }

        if (time1 % 60 == 0) {
            atackRound();
        }

        if (time1 % 180 == 0) {
            do {
                newX = Game.player.getX() + generator.nextInt(50) - 25;
                newY = Game.player.getY() + generator.nextInt(50) - 25;
                newX = newX >> 4;
                newY = newY >> 4;       //musi byc wspolrzedne podzelen przez 16 by znajdowal sie on w calosci na jednym tilu
                newX = newX << 4;
                newY = newY << 4;
            } while (collisionResp(newX, newY));
            teleport(newX, newY);
            time1 = 0;
        }
    }

    private void atackPlayer() {
        double dx = Game.player.getX() - 9 - x;
        double dy = Game.player.getY() - 10 - y;          //zobaczymy jak bedzie z kolizja z mobami
        double dirProjectile = Math.atan2(dy, dx);
        enemyShoot(x + 10, y + 15, dirProjectile);
    }

    private void atackRound() {
        for (int i = 0; i < 8; i++) {
            double dirProjectile = -Math.PI + ((double) 2 / 8 * Math.PI * i);
            enemyShoot(x + 5, y + 6, dirProjectile);
        }
    }

    private void lastChance() {
        if (HP <= 20) {
            if (healingAbility) {
                if (generator.nextInt(2) == 0) {
                    HP = MAX_HP / 2;
                } else
                    healingAbility = false;
            }
        }
    }

    /**
     * Perfoms death of boss and also heals game hero.
     */
    public void death() {
        Game.player.gainHP(Game.player.getMAX_HP());
    }
}