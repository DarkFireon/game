package entity.mob.enemy;

import entity.mob.Mob;
import entity.mob.Player;
import game.Game;
import graphics.AnimatedSprite;
import graphics.Screen;
import graphics.SpriteSheet;

import java.util.Random;

public abstract class Enemy extends Mob {

    Random generator = new Random();
    private AnimatedSprite down = new AnimatedSprite(SpriteSheet.dummy_down, 16, 16, 3);
    AnimatedSprite animSprite = down;
    private AnimatedSprite up = new AnimatedSprite(SpriteSheet.dummy_up, 16, 16, 3);
    private AnimatedSprite left = new AnimatedSprite(SpriteSheet.dummy_left, 16, 16, 3);
    private AnimatedSprite right = new AnimatedSprite(SpriteSheet.dummy_right, 16, 16, 3);

    /**
     * Spawn enemy on the given screen
     *
     * @param screen given screen to place enemy there
     */
    public void render(Screen screen) {
        sprite = animSprite.getSprite();
        screen.renderMob(x, y, this);
    }

    /**
     * Measure actual distance from enemy to player
     *
     * @return ditance to player as double number
     */
    double distanceToPlayer() {
        Player player = Game.player;
        int dx = x - player.getX();
        int dy = y - player.getY();

        return Math.sqrt((Math.pow(dx, 2) + Math.pow(dy, 2)));
    }

    /**
     * Move enemy with given distance.
     *
     * @param xa position
     * @param ya position
     */
    void walk(int xa, int ya) {
        if (ya < 0) {
            dir = Direction.UP;
            animSprite = up;
        }
        if (ya > 0) {
            animSprite = down;
            dir = Direction.DOWN;
        }
        if (xa < 0) {
            animSprite = left;
            dir = Direction.LEFT;
        }
        if (xa > 0) {
            animSprite = right;
            dir = Direction.RIGHT;
        }

        if (xa != 0 || ya != 0) {
            move(xa, ya);
            walking = true;
        } else {
            walking = false;
        }
    }
}
