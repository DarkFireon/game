package entity.mob.enemy;

import audio.Muzyka;
import entity.mob.Mob;
import entity.mob.Player;
import game.Game;
import graphics.Sprite;
import level.Node;
import utilities.Vector2i;

import java.util.List;

public class Skeleton extends Enemy {
    private Player player = Game.player;
    private List<Node> path = null;
    private int xa, ya;
    private int time = 0;
    private int time1 = 0;
    private int slow = 1;
    private Mob owner = this;
    private int rewardHP = 10;
    private int damage;

    /**
     * Creates a Skeleton instance at given position and at given stage.
     *
     * @param x position
     * @param y position
     * @param stageNumber number of game stage (e.g. dangeons)
     */
    public Skeleton(int x, int y, int stageNumber) {
        this.x = x << 4;
        this.y = y << 4;
        sprite = Sprite.skeleton;
        HP = 80;
        MAX_HP = 80;
        damage = 10;
        init(Game.level);
        this.stageNumber = stageNumber;
    }

    /**
     * Skeleton could have it's owner who rules over him.
     *
     * @return Mob current owner
     */
    Mob getOwner() {
        return owner;
    }

    /**
     * Skeleton could have it's owner who rules over him.
     *
     * @param mob instance of mob which will be owner.
     */
    void setOwner(Mob mob) {
        owner = mob;
        rewardHP = 50;
        slow = 2;
    }

    /**
     * Change sceleton's behaviur on game board. It means it's position and it performs attack on game hero.
     */
    public void update() {
        time++;
        time1++;
        ya = xa = 0;

        if (walking) animSprite.update();
        else animSprite.setFrame(0);

        Vector2i start = new Vector2i(getX() >> 4, getY() >> 4);  //dzielimy o 16 potrzebujemy precyzji w tilach
        Vector2i destination = new Vector2i(Game.player.getX() + 3 >> 4, Game.player.getY() >> 4);
        if (time1 % 3 == 0 && distanceToPlayer() < 250)
            path = level.findPath(start, destination);

        if (path != null && distanceToPlayer() < 250) {
            if (path.size() > 0) {
                Vector2i vec = path.get(path.size() - 1).tile;
                if (x < vec.getX() << 4) xa++;
                if (x > vec.getX() << 4) xa--;
                if (y < vec.getY() << 4) ya++;
                if (y > vec.getY() << 4) ya--;
            }
        }

        if (time % slow == 0)
            walk(xa, ya);

        if (distanceToPlayer() < 45 && time % 60 == 0) {
            Game.player.loseHP(damage);
            time = 0;
        }
    }

    /**
     * Performs death of skeleton and heals game hero with determined amount of health points.
     */
    public void death() {

        Game.player.gainHP(rewardHP);
        Muzyka.play(3);
    }

}
