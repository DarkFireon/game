package entity.mob.enemy;

import graphics.Sprite;
import level.Level;

public class RedSkeleton extends Enemy {


    private int xa = 0, ya = 0;
    private int maxCreation = 25;
    private int time = 60, time1 = 100;

    /**
     * Creates a RedSkeleton instance at given position and at given stage.
     *
     * @param x position
     * @param y position
     * @param stageNumber number of game stage (e.g. dangeons)
     */
    public RedSkeleton(int x, int y, int stageNumber) {
        //init(Game.level);
        this.x = x << 4;
        this.y = y << 4;
        sprite = Sprite.skeleton;
        HP = 320;
        MAX_HP = 320;
        this.stageNumber = stageNumber;
    }

    /**
     * Updates RedSkeleton's position on the screen.
     */
    public void update() {
        time--;

        if (walking) animSprite.update();
        else animSprite.setFrame(1);

        if (distanceToPlayer() < 200) {
            time1--;
            if (time1 == 0 && maxCreation > 1) {
                int x = this.x + (generator.nextInt(3) - 1) * 16;
                int y = this.y + (generator.nextInt(3) - 1) * 16;
                if (!collisionResp(x, y)) {
                    Level.add(new Skeleton(x / 16, y / 16, stageNumber));
                    maxCreation--;
                }
                time1 = 100;
            }
        }

        if (time == 0) {
            xa = generator.nextInt(3) - 1;
            ya = generator.nextInt(3) - 1;
            time = 60;
        }

        walk(xa, ya);
    }
}