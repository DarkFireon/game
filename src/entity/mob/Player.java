package entity.mob;


import entity.projectile.Projectile;
import entity.projectile.WizardProjectile;
import game.Game;
import game.GameOverFrame;
import graphics.AnimatedSprite;
import graphics.Screen;
import graphics.Sprite;
import graphics.SpriteSheet;
import input.Keyboard;
import input.Mouse;


public class Player extends Mob {
    private Keyboard input;
    private Sprite sprite;

    private AnimatedSprite down = new AnimatedSprite(SpriteSheet.player_down,16,16,3);
    private AnimatedSprite up = new AnimatedSprite(SpriteSheet.player_up,16,16,3);
    private AnimatedSprite left = new AnimatedSprite(SpriteSheet.player_left,16,16,3);
    private AnimatedSprite right = new AnimatedSprite(SpriteSheet.player_right,16,16,3);

    private  AnimatedSprite animSprite=down;
    private int fireRate;

    /**
     * Creates a Player instance at given position and at given stage.
     *
     * @param x position
     * @param y position
     * @param input instance of KeyListener subclass implemented by us.
     */
    public Player(int x,int y,Keyboard input){
        this.x=x;
        this.y=y;
        this.input=input;
        fireRate=WizardProjectile.FIRE_RATE;
        HP = 800;
        MAX_HP = 800;
        //HP = 100;
        //MAX_HP = 100;
        stageNumber = 1;
    }

    /**
     * Changes armor, clothhes worn by player.
     */
    public void clothsChange() {
        down = new AnimatedSprite(SpriteSheet.player2_down, 16, 16, 3);
        up = new AnimatedSprite(SpriteSheet.player2_up, 16, 16, 3);
        left = new AnimatedSprite(SpriteSheet.player2_left, 16, 16, 3);
        right = new AnimatedSprite(SpriteSheet.player2_right, 16, 16, 3);
    }


    /**
     * Change player's behaviur on game board. It means it's position and it performs attack on game hero.
     */
    public void update() {
        if(fireRate>0)fireRate--;

        if(walking)animSprite.update();
        else animSprite.setFrame(1);
        int xa = 0, ya = 0;
        if (input.up) {
            animSprite=up;
            ya--;
        }
        if (input.down){
            animSprite=down;
            ya++;
        }
        if (input.right) {
            animSprite=right;
            xa++;
        }
        if (input.left) {
            animSprite=left;
            xa--;
        }
        if ((xa != 0 || ya != 0)&&HP>0) {
        if(input.shift)
            movePlayer( 5*xa, 5* ya);

        else
            movePlayer(2*xa, 2*ya);
            walking=true;
        }
        else{
            walking=false;
        }

        clear();
        updateShooting();

        if (HP <= 0)
            death();
    }

    /**
     * Performs death event
     */
    public void death() {
        Game.running = false;
        GameOverFrame.wygrana = false;
    }

    private void clear() {
        for(int i=0;i<level.getProjectiles().size();i++){
            Projectile p=level.getProjectiles().get(i);
            if(p.isRemoved())level.getProjectiles().remove(i);
        }
    }

    private void updateShooting() {
        if(Mouse.getButton()==1&& fireRate<=0) {
            double dx = Mouse.getX() - 9 - (Game.getWindowWidth() / 2 - 8 - 3 * Game.cameraShiftX);
            double dy = Mouse.getY() - 10 - (Game.getWindowHeight() / 2 - 8 - 3 * Game.cameraShiftY);
            double dirProjectile=Math.atan2(dy,dx);
            if(HP>0)
            shoot(x+5, y+6, dirProjectile);
            fireRate= WizardProjectile.FIRE_RATE;
        }
    }

    /**
     * Renders mob on the screen.
     */
    public void render(Screen screen){
        sprite=animSprite.getSprite();
        screen.renderMob(x,y,sprite,this);
    }
}
