package entity;

import graphics.Screen;
import graphics.Sprite;
import level.Level;

import java.util.Random;

public abstract class Entity {

    protected final Random random = new Random();
    public int x, y;
    protected Sprite sprite;
    protected Level level;
    private boolean removed = false;
    protected int stageNumber;


    /**
     * Returns number of actual stage on which entity is located.
     */
    public int getStageNumber() {
        return stageNumber;
    }

    /**
     * Set stage for the entity.
     *
     * @param stageNumber number of stage
     */
    public void setStageNumber(int stageNumber) {
        this.stageNumber = stageNumber;
    }

    /**
     * Abstract method to update stuff in derived classes.
     */
    public abstract void update();

    /**
     * Abstract method to render entity declard in derived classes.
     */
    public abstract void render(Screen screen);

    /**
     * Returns actual sprite of entity.
     */
    public Sprite getSprite() {
        return sprite;
    }

    /**
     * Removes entity from level.
     */
    public void remove() {
        removed = true;
    }

    /**
     * Check if the entity was removed.
     */
    public boolean isRemoved() {
        return removed;
    }

    /**
     * Init entity on given level
     *
     * @param level level to spawn entity there
     */
    public void init(Level level) {
        this.level = level;
    }

    /**
     * Returns entity's x coordinate.
     */
    public int getX() {
        return x;
    }

    /**
     * Returns entity's y coordinate.
     */
    public int getY() {
        return y;
    }

}
