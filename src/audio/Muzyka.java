package audio;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class Muzyka {

    /**
     * Play a audio file at given number
     *
     * @param x file number of sudio file to play
     */
    public static void play(int x) {

            try {


                File audio = new File("kids.wav");
                if(x == 1)
                    audio = new File("src/audio/voices/witaj.wav");
                if(x == 0)
                    audio = new File("src/audio/music/play2.wav");
                if(x == 2)
                    audio = new File("src/audio/music/time.wav");
                if(x == 3)
                    audio = new File("src/audio/voices/death.wav");
                if(x == 4)
                    audio = new File("src/audio/voices/allah.wav");

                AudioInputStream audioIn = AudioSystem.getAudioInputStream(audio);
                // Get a sound clip resource.
                Clip clip = AudioSystem.getClip();
                // Open audio clip and load samples from the audio input stream.
                clip.open(audioIn);
                clip.start();
            } catch (UnsupportedAudioFileException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            }

    }


}
